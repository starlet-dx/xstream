%bcond_with jp_minimal
Name:                xstream
Version:             1.4.21
Release:             1
Summary:             Java XML serialization library
License:             BSD-3-Clause
URL:                 http://x-stream.github.io/
BuildArch:           noarch
Source0:             https://repo1.maven.org/maven2/com/thoughtworks/%{name}/%{name}-distribution/%{version}/%{name}-distribution-%{version}-src.zip

BuildRequires:       maven-local mvn(cglib:cglib) mvn(dom4j:dom4j) mvn(javax.xml.bind:jaxb-api)
BuildRequires:       mvn(joda-time:joda-time) mvn(net.sf.kxml:kxml2-min)
BuildRequires:       mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-enforcer-plugin)
BuildRequires:       mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires:       mvn(org.codehaus.woodstox:woodstox-core-asl) mvn(org.jdom:jdom)
BuildRequires:       mvn(org.jdom:jdom2) mvn(stax:stax) mvn(stax:stax-api) mvn(xpp3:xpp3)
BuildRequires:       mvn(xpp3:xpp3_min) maven mvn(org.codehaus.mojo:build-helper-maven-plugin)
%if %{without jp_minimal}
BuildRequires:       mvn(javassist:javassist) mvn(org.codehaus.jettison:jettison)
BuildRequires:       mvn(org.hibernate:hibernate-core) mvn(org.hibernate:hibernate-envers)
BuildRequires:       mvn(org.slf4j:slf4j-simple) mvn(xom:xom) mvn(io.github.x-stream:mxparser)
%endif
%description
XStream is a simple library to serialize objects to XML
and back again. A high level facade is supplied that
simplifies common use cases. Custom objects can be serialized
without need for specifying mappings. Speed and low memory
footprint are a crucial part of the design, making it suitable
for large object graphs or systems with high message throughput.
No information is duplicated that can be obtained via reflection.
This results in XML that is easier to read for humans and more
compact than native Java serialization. XStream serializes internal
fields, including private and final. Supports non-public and inner
classes. Classes are not required to have default constructor.
Duplicate references encountered in the object-model will be
maintained. Supports circular references. By implementing an
interface, XStream can serialize directly to/from any tree
structure (not just XML). Strategies can be registered allowing
customization of how particular types are represented as XML.
When an exception occurs due to malformed XML, detailed diagnostics
are provided to help isolate and fix the problem.

%package        javadoc
Summary:        Javadoc for xstream
%description    javadoc
xstream API documentation.
%if %{without jp_minimal}

%package        hibernate
Summary:        hibernate module for xstream
Requires:       xstream = %{version}-%{release}
%description    hibernate
hibernate module for xstream.
%endif

%package        benchmark
Summary:        benchmark module for xstream
Requires:       xstream = %{version}-%{release}
%description    benchmark
benchmark module for xstream.

%package        parent
Summary:        Parent POM for xstream
Requires:       xstream = %{version}-%{release}
%description parent
Parent POM for xstream.

%prep
%autosetup -n xstream-%{version} -p1

sed -i "s/3.2.7/4.0.0/g" pom.xml
find . -name "*.class" -print -delete
find . -name "*.jar" -print -delete
%pom_disable_module xstream-distribution
%pom_disable_module xstream-jmh
%pom_remove_plugin :maven-source-plugin
%pom_remove_plugin :maven-dependency-plugin
%pom_remove_plugin :maven-eclipse-plugin
%pom_remove_plugin :maven-release-plugin
%pom_remove_plugin :xsite-maven-plugin
%pom_remove_plugin :maven-compiler-plugin
%pom_xpath_set "pom:dependency[pom:groupId = 'org.codehaus.woodstox' ]/pom:artifactId" woodstox-core-asl
%pom_xpath_set "pom:dependency[pom:groupId = 'org.codehaus.woodstox' ]/pom:artifactId" woodstox-core-asl xstream
%pom_xpath_set "pom:dependency[pom:groupId = 'cglib' ]/pom:artifactId" cglib
%pom_xpath_set "pom:dependency[pom:groupId = 'cglib' ]/pom:artifactId" cglib xstream
%pom_remove_plugin :maven-antrun-plugin
%pom_remove_plugin :maven-dependency-plugin xstream
%pom_remove_plugin :maven-javadoc-plugin xstream
%pom_remove_dep javax.activation:activation xstream
%pom_xpath_set "pom:project/pom:dependencies/pom:dependency[pom:groupId = 'cglib' ]/pom:artifactId" cglib xstream-hibernate
%pom_xpath_inject "pom:project/pom:dependencies/pom:dependency[pom:groupId = 'junit' ]" "<scope>test</scope>" xstream-hibernate
%pom_remove_plugin :maven-dependency-plugin xstream-hibernate
%pom_remove_plugin :maven-javadoc-plugin xstream-hibernate
%pom_xpath_inject "pom:project/pom:dependencies/pom:dependency[pom:groupId = 'junit' ]" "<scope>test</scope>" xstream-benchmark
%pom_remove_plugin :maven-javadoc-plugin xstream-benchmark
%if %{with jp_minimal}
%pom_disable_module xstream-hibernate
%pom_remove_dep -r xom:xom
%pom_remove_dep -r org.codehaus.jettison:jettison
%pom_remove_dep org.codehaus.woodstox:woodstox-core-asl xstream
rm xstream/src/java/com/thoughtworks/xstream/io/xml/Xom*
rm xstream/src/java/com/thoughtworks/xstream/io/json/Jettison*
rm xstream-benchmark/src/java/com/thoughtworks/xstream/tools/benchmark/products/XStreamXom.java
%endif
%mvn_file :xstream xstream/xstream xstream
%mvn_file :xstream-benchmark xstream/xstream-benchmark xstream-benchmark
%mvn_package :xstream

%build
%mvn_build -f -s -- -Dversion.java.source=8

%install
%mvn_install

%files -f .mfiles
%doc README.txt
%license LICENSE.txt

%files parent -f .mfiles-xstream-parent
%if %{without jp_minimal}

%files hibernate -f .mfiles-xstream-hibernate
%endif

%files benchmark -f .mfiles-xstream-benchmark

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt

%changelog
* Mon Nov 11 2024 yaoxin <yao_xin001@hoperun.com> - 1.4.21-1
- Update to 1.4.21
  * Security fixes
    - This maintenance release addresses the security vulnerability
      CVE-2024-47072 (bsc#1233085), when using the BinaryDriver to
      unmarshal a manipulated input stream causing a Denial of
      Service due to a stack overflow.
  * Major changes
    - #350: Optimize memory allocation
    - Add a converter for the WeakHashMap which does not write any
      elements of the map. Avoids also access to the ReentrantLock
      contained in the WeakHashMap since Java 19.
  * Minor changes
    - #335: Allow PrettyPrintWriter to replace invalid XML
      characters when not running in quirks mode
    - #331, #326: Fix handling of empty
      java.util.concurrent.atomic.AtomicReference
    - #334: Fix remaining buffer size calculation in QuickWriter
    - #342: Optimize internal handling of children in DomReader
      avoiding O(n^2) access times for siblings
    - #349: Fix support of lambda objects for Java 21 and above
    - #359: Add KEYS file with public keys to verify signed
      artifacts.
    - Detect input manipulation in
      c.t.x.io.binary.BinaryStreamReader.
    - Use Jettison 1.5.4 by default for Java Runtimes version 8 or
      higher.
  * API changes
    - Added constant
      c.t.x.io.xml.PrettyPrintWriter.XML_1_0_REPLACEMENT.
    - Added constant
      c.t.x.io.xml.PrettyPrintWriter.XML_1_1_REPLACEMENT.
    - Added c.t.x.converters.collections.WeakHashMapConverter.
    - Protected field fieldsToOmit of
      c.t.x.mapper.ElementIgnoringMapper set to private.
    - Protected field unknownElementsToIgnore of
      c.t.x.mapper.ElementIgnoringMapper set to private.
  * Stream compatibility
    - The WeakHashMaps, that have been written with previous
      versions of XStream, can still be deserialized.

* Mon Dec 11 2023 yaoxin <yao_xin001@hoperun.com> - 1.4.20-1
- Upgrade to 1.4.20 for fix CVE-2022-40151 and CVE-2022-41966

* Mon Feb 7 2022 wangkai <wangkai385@huawei.com> - 1.4.18-2
- Fix CVE-2021-43859

* Sat Sep 4 2021 houyingchao <houyingchao@huawei.com> - 1.4.18-1
- Upgrade 1.4.18 to fix cves

* Thu 22 Jul 2021 sunguoshuai <sunguoshuai@huawei.com> - 1.4.17-2
- Change setting.xml to huaweicloud

* Thu May 27 2021 wutao <wutao61@huawei.com> - 1.4.17-1
- upgrade to 1.4.17 to fix CVE-2021-29505

* Tue Mar 30 2021 wutao <wutao61@huawei.com> - 1.4.16-1
- update to 1.4.16

* Mon Jan 11 2021 wangyue<wangyue92@huawei.com>-1.4.11.1-3
- Fix CVE-2020-26258 CVE-2020-26259

* Sat Dec 12 2020 huanghaitao <huanghaitao8@huawei.com> - 1.4.11.1-2
- Fix CVE-2020-26217 CVE-2017-9805

* Fri Aug 14 2020 yaokai <yaokai13@huawei.com> - 1.4.11.1-1
- upgrade to 1.4.11.1-1

* Mon Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.4.9-9
- Package init
